package ir.phsys.randd.xmpp;

/**
 * @author pooya
 *         date : 8/30/16
 */
public class ClientSender1 {

    public static void main(String[] args) throws Exception {

        String username = "pooya";
        String password = "123456";

        XmppManager xmppManager = new XmppManager("192.168.104.129", 5222);

        xmppManager.init();
        xmppManager.performLogin(username, password);
        xmppManager.setStatus(true, "Hello everyone");

        String buddyJID = "mostafa";
        String buddyName = "mostafa";
        xmppManager.createEntry(buddyJID, buddyName);

        xmppManager.sendMessage("Hello mate", buddyJID+"@pooya-pc");

        boolean isRunning = true;

        while (isRunning) {
            Thread.sleep(50);
        }
        xmppManager.destroy();
    }
}