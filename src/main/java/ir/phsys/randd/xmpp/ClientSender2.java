package ir.phsys.randd.xmpp;

/**
 * @author pooya
 *         date : 8/30/16
 */
public class ClientSender2{

    public static void main(String[] args) throws Exception {

        String username = "mostafa";
        String password = "123456";

        XmppManager xmppManager = new XmppManager("pooya-pc", 5222);

        xmppManager.init();
        xmppManager.performLogin(username, password);

        xmppManager.setStatus(true, "Hello everyone");

        String buddyJID = "pooya";
        String buddyName = "pooya";
        xmppManager.createEntry(buddyJID, buddyName);

        for (int i = 0; i < 10; i++) {
            xmppManager.sendMessage("Hello mate" , buddyJID + "@pooya-pc");
        }

        xmppManager.setStatus(true,"I'm busy");

        boolean isRunning = true;

        while (isRunning) {
            Thread.sleep(50);
        }

        xmppManager.destroy();

    }

}