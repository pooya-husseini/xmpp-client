package ir.phsys.randd.xmpp;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Type;

import java.util.HashMap;
import java.util.Map;

public class XmppManager {

    private static final int packetReplyTimeout = 500; // millis

    private String server;
    private int port;

    private ConnectionConfiguration config;
    private XMPPConnection connection;
    private AccountManager accountManager;
    private ChatManager chatManager;
    private MessageListener messageListener;

    public XmppManager(String server, int port) {
        this.server = server;
        this.port = port;
    }

    public void init() throws XMPPException {

        System.out.println(String.format("Initializing connection to server %1$s port %2$d", server, port));

        SmackConfiguration.setPacketReplyTimeout(packetReplyTimeout);

        config = new ConnectionConfiguration(server, port);

        config.setSASLAuthenticationEnabled(false);
        config.setSecurityMode(SecurityMode.disabled);

        connection = new XMPPConnection(config);
        connection.connect();


        System.out.println("Connected: " + connection.isConnected());

        chatManager = connection.getChatManager();

        messageListener = new MyMessageListener();

        accountManager = connection.getAccountManager();
    }

    public void createUser(String name,String password) throws XMPPException {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", name);
        if (accountManager.supportsAccountCreation()) {
            accountManager.createAccount(name, password,map);
        }else{
            throw new IllegalArgumentException();
        }
    }

    public void performLogin(String username, String password) throws XMPPException {
        if (connection != null && connection.isConnected()) {
            connection.login(username, password);
        }
        chatManager.addChatListener(new ChatManagerListener() {
            public void chatCreated(Chat chat, boolean createdLocally) {
                chat.addMessageListener(messageListener);
            }
        });
    }

    public void setStatus(boolean available, String status) {

        Presence.Type type = available ? Type.available : Type.unavailable;
        Presence presence = new Presence(type);

        presence.setStatus(status);
        connection.sendPacket(presence);

    }

    public void destroy() {
        if (connection != null && connection.isConnected()) {
            connection.disconnect();
        }
    }

    public void sendMessage(String message, String buddyJID) throws XMPPException {
        System.out.println(String.format("Sending mesage '%1$s' to user %2$s", message, buddyJID));
        Chat chat = chatManager.createChat(buddyJID, messageListener);

        chat.sendMessage(message);
        chat.addMessageListener(messageListener);
    }

    public void createEntry(String user, String name) throws Exception {
        System.out.println(String.format("Creating entry for buddy '%1$s' with name %2$s", user, name));
        Roster roster = connection.getRoster();
        roster.createEntry(user, name, null);
    }

    class MyMessageListener implements MessageListener {

        public void processMessage(Chat chat, Message message) {
            String from = message.getFrom();
            String body = message.getBody();
            System.out.println(String.format("Received message '%1$s' from %2$s", body, from));
        }
    }
}
